<?php

namespace Drupal\announcements\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Announcement entities.
 *
 * @ingroup announcements
 */
class AnnouncementDeleteForm extends ContentEntityDeleteForm {


}
