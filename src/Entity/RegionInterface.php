<?php

namespace Drupal\announcements\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Announcement Region entities.
 */
interface RegionInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
