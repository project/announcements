<?php

namespace Drupal\announcements;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for announcements.
 */
class AnnouncementTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
